import java.util.ArrayList;

public class FQDN extends Hexable {
    // Holds the FQDN in period delimited form
    private String data;
    /*
     * Constructor
     *   Takes in a string, ending with the root node or not,
     *   and initializes an instance of FQDN to that FQDN
     */
    public FQDN(String input) {
        data = input.toLowerCase();
        while (data.endsWith(".")) {
            data = data.substring(0,input.length()-1);
        }
    }
    /*
     * decode Method (Static)
     *   Takes in a byte array representing an FQDN and parses out
     *   the string representation of that FQDN and returns a new instance
     *   of an FQDN object initialized to that FQDN
     *      NOTE: Throws DecodeException on ArrayIndexOutOfBoundsException
     */
    public static FQDN decode(byte[] input, int offset, int length) throws DecodeException {
        String resultStr = new String();
        int index = offset;
        ArrayList<Integer> visited = new ArrayList<Integer>();
        visited.add(index);
        int currSize = 0;
        char tempByte = 0;
        char[] currZone = new char[0];
        try {
            // 11XX XXXX
            tempByte = (char)input[index];
            while ((tempByte & 0xC0) == 192) {
	            // XX11 1111 1111 1111
	            index = (tempByte & 0x3F) * 128 + (input[index+1] & 0xFF);
	            if (visited.contains(index))
	            	throw new DecodeException("Pointer loop detected!",1);
	            visited.add(index);
	            tempByte = (char)input[index];
            }
            currSize = (input[index] & 0xFF);
            while (currSize > 0) {
                if (currSize > 63)
                    throw new DecodeException("FQDN Exceeds 63 Octets!");
            	index++;
            	currZone = new char[currSize];
	            for (int idx = 0; idx < currSize; idx++) {
		            if (((input[index+idx] > 47) && (input[index+idx] < 58)) ||
	                    ((input[index+idx] > 64) && (input[index+idx] < 91)) ||
	                    ((input[index+idx] > 96) && (input[index+idx] < 123)) ||
                        ((input[index+idx] == 45))) {
	                    currZone[idx] = (char)input[index+idx];
	                } else {
		                throw new DecodeException("Invalid Character Used in FQDN starting at byte: " + index + 
                            " idx: " + idx + " (" + length + " Bytes) = " + (char)input[offset+index+idx]);
	                }
	            }
	            index += currSize;
	            resultStr = resultStr + new String(currZone) + ".";
	            tempByte = (char)input[index];
	            while ((tempByte & 0xC0) == 192) {
		            index = (tempByte & 0x3F) * 128 + (input[index+1] & 0xFF);
		            if (visited.contains(index))
		            	throw new DecodeException("Pointer loop detected!",1);
		            visited.add(index);
		            tempByte = (char)input[index];
	            }
	            currSize = (input[index] & 0xFF);
	        }
            return new FQDN(resultStr);
        } catch (ArrayIndexOutOfBoundsException ex) {
            throw new DecodeException("Error Decoding FQDN starting at byte: " + index + " (" + length + 
                " Bytes) Result: " + resultStr);
        }
    }
    /*
     * encode Method
     *   Returns this instance's FQDN in byte array DNS datagram format,
     *   namely ([size][char bytes])*0
     */
    @Override
    public byte[] encode() {
        byte[] ret = new byte[data.length()+2];
        String[] zones = data.split("\\.");
        int index = 0;
        for (int zone = 0; zone < zones.length; zone++) {
            ret[index] = (byte)zones[zone].length();
            index++;
            for (int zoneIdx = 0; zoneIdx < zones[zone].length(); zoneIdx++) {
                ret[index+zoneIdx] = (byte)zones[zone].charAt(zoneIdx);
            }
            index += zones[zone].length();
        }
        ret[index] = 0;
        return ret;
    }
    public String identify() {
    	return data + ".";
    }
}

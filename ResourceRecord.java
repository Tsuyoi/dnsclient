public class ResourceRecord extends Hexable {
	private FQDN rrName;
	private int rrType;
	private int rrClass;
	private int rrTTL;
	private int rrRDLength;
	private byte[] rrRData;
	private FQDN rrRDataFQDN;
	public ResourceRecord() {}
	public ResourceRecord(FQDN NAME, int TYPE, int CLASS, int TTL, int RDLENGTH, 
		byte[] RDATA, FQDN RRRDATAFQDN) {
		rrName = NAME;
		rrType = TYPE;
		rrClass = CLASS;
		rrTTL = TTL;
		rrRDLength = RDLENGTH;
		rrRData = new byte[RDATA.length];
		for (int index = 0; index < RDATA.length; index++)
			rrRData[index] = RDATA[index];
		rrRDataFQDN = RRRDATAFQDN;
	}
	public static ResourceRecord decode(byte[] input, int offset, int length) throws DecodeException {
		int pointer = offset;
		try {
			FQDN RRName = FQDN.decode(input, pointer, 0);
			char tempByte = (char)input[pointer];
			if ((tempByte & 0xC0) == 192) {
				pointer += 2;
			} else {
				while (input[pointer] > 0)
					pointer++;
				pointer++;
			}
			int RRType = 0;
			for (int index = 0; index < 2; index++) {
				int shift = (1 - index) * 8;
				RRType += (input[pointer+index] & 0x00FF) << shift;
			}
			pointer += 2;
			int RRClass = 0;
			for (int index = 0; index < 2; index++) {
				int shift = (1 - index) * 8;
				RRClass += (input[pointer+index] & 0x00FF) << shift;
			}
			pointer += 2;
			switch (RRClass) {
				case 1:
					break;
				default:
					throw new DecodeException("Invalid Class in Resource Record starting at byte: " + 
						offset + " (" + length + " Bytes)");
			}
			int RRttl = 0;
			for (int index = 0; index < 4; index++) {
				int shift = (3 - index) * 8;
				RRttl += (input[pointer+index] & 0x000000FF) << shift;
			}
			pointer += 4;
			int RRRDLength = 0;
			for (int index = 0; index < 2; index++) {
				int shift = (1 - index) * 8;
				RRRDLength += (input[pointer+index] & 0x00FF) << shift;
			}
			if (RRRDLength > 255)
				throw new DecodeException("RDLEN Indicates RDATA Exceeds 255 Octet Maximum!",1);
			pointer += 2;
			byte[] RRRData = new byte[RRRDLength];
			for (int index = 0; index < RRRDLength; index++)
				RRRData[index] = input[pointer+index];
			FQDN RRRDataFQDN = new FQDN("blank");
			switch (RRType) {
				case 1:
					if (RRRDLength > 4)
						throw new DecodeException("RDLength > 4 for an IP Address");
					break;
				case 2:
				case 5:
					RRRDataFQDN = FQDN.decode(input,pointer,0);
					break;
				default:
					throw new DecodeException("Invalid Type in Resource Record starting at byte: " + 
						offset + " (" + length + " Bytes)");
			}
			return new ResourceRecord(RRName, RRType, RRClass, RRttl, RRRDLength, RRRData, RRRDataFQDN);
		} catch (DecodeException ex) {
			throw ex;
		} catch (ArrayIndexOutOfBoundsException ex) {
			throw new DecodeException("Error decoding Resource Record starting at byte: " + pointer + 
				" (" + length + " Bytes)");
		}
	}
	@Override
	public byte[] encode() {
		byte[] name = rrName.encode();
		byte[] ret = new byte[name.length+10+rrRData.length];
		for (int index = 0; index < name.length; index++)
			ret[index] = name[index];
		int offset = name.length;
		for (int index = 0; index < 2; index++)
			ret[offset+index] = (byte)(rrType >>>((1-index) * 8));
		offset += 2;
		for (int index = 0; index < 2; index++)
			ret[offset+index] = (byte)(rrClass >>>((1-index) * 8));
		offset += 2;
		for (int index = 0; index < 4; index++)
			ret[offset+index] = (byte)(rrTTL >>>((3-index) * 8));
		offset += 4;
		for (int index = 0; index < 2; index++)
			ret[offset+index] = (byte)(rrRDLength >>>((1-index) * 8));
		offset += 2;
		for (int index = 0; index < rrRData.length; index++)
			ret[offset+index] = rrRData[index];
		return ret;
	}
	public String identify(){
		String ret = new String();
    	ret = ret + rrName.identify();
    	switch(rrType) {
	    	case 1:
	    		ret = ret + "  A";
	    		break;
	    	case 2:
	    		ret = ret + "  NS";
	    		break;
	    	case 5:
	    		ret = ret + "  CNAME";
	    		break;
	    	case 15:
	    		ret = ret + "  MX";
	    		break;
	    	default:
	    		ret = ret + " UNKNOWN";
	    		break;
    	}
    	switch(rrClass) {
	    	case 1:
	    		ret = ret + "  IN";
	    		break;
	    	default:
	    		ret = ret + "  UNKNOWN";
	    		break;
    	}
    	ret = ret + "  " + rrTTL + "  [" + rrRDLength + "]";
    	switch (rrType) {
	    	case 1:
	    		ret = ret + "  " + (rrRData[0] & 0xFF) + "." + (rrRData[1] & 0xFF) + "." + 
	    			(rrRData[2] & 0xFF) + "." + (rrRData[3] & 0xFF);
	    		break;
	    	case 2:
	    	case 5:
	    		ret = ret + "  " + rrRDataFQDN.identify();
	    		break;
    	}
    	return ret;
	}
	public String getRData() {
		String ret = new String();
		switch (rrType) {
	    	case 1:
	    		ret = ret + (rrRData[0] & 0xFF) + "." + (rrRData[1] & 0xFF) + "." + 
	    			(rrRData[2] & 0xFF) + "." + (rrRData[3] & 0xFF);
	    		break;
	    	case 2:
	    	case 5:
	    		ret = ret + rrRDataFQDN.identify();
	    		break;
    	}
    	return ret;
	}
}
import java.lang.Exception;

public class CheckException extends Exception {
    private static final long serialVersionUID = 321L;
    private int cls = 0;
    public CheckException() { super(); }
    public CheckException(String message) { super(message); }
    public CheckException(String message, Throwable cause) {
        super(message, cause);
    }
    public CheckException(Throwable cause) { super(cause); }
    public CheckException(String message, int CLS) { super(message); cls = CLS; }
    public int getCls() { return cls; }
}

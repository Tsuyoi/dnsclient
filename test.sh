#! /bin/bash

baseClass=DNSClient
testIP='128.163.140.219'
testPort='47103'
testQuery=('nice.example.com' 'bad.example.com' 'ugly.example.com' 'nasty.example.com' 'dangerous.example.com' 'treacherous.fun.example.com')
header='\n**************************\nDNSClient Testing Script\n**************************\n'
clear
echo $header
printf "First let's clean up...\n"
make clean
printf "Now let's compile the sources...\n"
make
sleep 1
clear
echo $header
printf "Testing the base example\n"
java $baseClass
sleep 3
for query in "${testQuery[@]}"
  do
    clear
    echo $header
    java $baseClass $testIP $testPort "${query}"
    sleep 3
  done
clear
echo $header
java DNSClient 192.228.79.201 53 ozark.netlab.uky.edu
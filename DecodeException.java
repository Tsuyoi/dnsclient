import java.lang.Exception;

public class DecodeException extends Exception {
    private static final long serialVersionUID = 999L;
    private int cls = 0;
    public DecodeException() { super(); }
    public DecodeException(String message) { super(message); }
    public DecodeException(String message, Throwable cause) {
        super(message, cause);
    }
    public DecodeException(Throwable cause) { super(cause); }
    public DecodeException(String message, int CLS) { super(message); cls = CLS; }
    public int getCls() { return cls; }
}

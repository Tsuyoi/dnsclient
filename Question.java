public class Question extends Hexable {
    FQDN qname;
    int qtype;
    int qclass;
    public Question(FQDN QName, int QType, int QClass) {
        qname = QName;
        qtype = QType;
        qclass = QClass;
    }
    public static Question decode(byte[] input, int offset, int length) throws DecodeException {
        try {
        	int idx = offset;
        	FQDN QName = FQDN.decode(input, idx, 0);
        	idx += (length - 4);
			int QType = 0;
			for (int index = 0; index < 2; index++) {
				char temp = (char)input[idx+index];
				QType += (temp & 0x00FF) << ((1 - index) * 8);
			}
	        switch (QType) {
		        case 1:		// A
		        case 2:		// NS
		        case 5:		// CNAME
		        case 15:	// MX
		        	break;
		        default:
		        	throw new DecodeException("Error Decoding Question: INVALID QTYPE");
	        }
	        idx += 2;
	        int QClass = 0;
			for (int index = 0; index < 2; index++) {
				char temp = (char)input[idx+index];
				QClass += (temp & 0x00FF) << ((1 - index) * 8);
			}
	        switch (QClass) {
		        case 1:		// IN
		        	break;
		        default:
		        	throw new DecodeException("Error Decoding Question: INVALID QCLASS");
	        }
	        return new Question(QName, QType, QClass);
        } catch (DecodeException ex) {
            throw ex;
        } catch (ArrayIndexOutOfBoundsException ex) {
			throw new DecodeException("Error decoding Question starting at byte: " + offset + 
				" (" + length + " Bytes)");
		}
    }
    @Override
    public byte[] encode() {
        byte[] fqdn = qname.encode();
        byte[] ret = new byte[fqdn.length+4];
        for (int index = 0; index < fqdn.length; index++) {
            ret[index] = fqdn[index];
        }
        ret[ret.length-3] = (byte)qtype;
        ret[ret.length-1] = (byte)qclass;
        return ret;
    }
    public String identify() {
    	String ret = new String();
    	ret = ret + qname.identify();
    	switch(qtype) {
	    	case 1:
	    		ret = ret + "  A";
	    		break;
	    	case 2:
	    		ret = ret + "  NS";
	    		break;
	    	case 5:
	    		ret = ret + "  CNAME";
	    		break;
	    	case 15:
	    		ret = ret + "  MX";
	    		break;
	    	default:
	    		ret = ret + " UNKNOWN";
	    		break;
    	}
    	switch(qclass) {
	    	case 1:
	    		ret = ret + "  IN";
	    		break;
	    	default:
	    		ret = ret + "  UNKNOWN";
	    		break;
    	}
    	return ret;
    }
    public String getQuestionFQDN() {
    	return qname.identify();
    }
}

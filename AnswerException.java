import java.lang.Exception;

public class AnswerException extends Exception {
    private static final long serialVersionUID = 412L;
    private String addr;
    private int port;
    public AnswerException() { super(); }
    public AnswerException(String message) { super(message); }
    public AnswerException(String message, Throwable cause) {
        super(message, cause);
    }
    public AnswerException(Throwable cause) { super(cause); }
    public AnswerException(String ADDR, int PORT) { addr=ADDR;port=PORT; }
    public String getAddr() { return addr; }
    public int getPort() { return port; }
}

import java.io.*;
import java.net.*;
import java.lang.Math;
import java.util.ArrayList;

public class DNSClient {
    public static void main(String args[]) throws Exception {
        String lookup = new String();
        String server = new String();
        ArrayList<String> history = new ArrayList<String>();
        final int MAXTRIES = 10;
        int port = 0;
        if (args.length == 3) {
            server = args[0];
            try {
            	port = Integer.parseInt(args[1]);
            } catch (NumberFormatException ex) {
            	System.out.println(ex);
            	System.out.println("Using default port of 53");
            	port = 53;
            }
            lookup = args[2];
        } else if (args.length == 0) {
            lookup = new String("ozark.netlab.uky.edu");
            server = new String("128.163.140.42");
            port = 53;
        } else {
            System.out.println("Correct Usage: [DNS] [Port] [FQDN]");
            System.exit(1);
        }
        System.out.println("Created Socket!");
        InetAddress dns = InetAddress.getByName(server);
        InetAddress old_dns = dns;
        DatagramSocket socket = new DatagramSocket();
        socket.setSoTimeout(1000);
        byte[] buffer = new byte[1000];
        DatagramPacket received = new DatagramPacket(buffer, buffer.length);
        boolean answered = false;
        int cycle = 0;
        DNSMsg msg = new DNSMsg(lookup);
        buffer = msg.encode();
        DatagramPacket out = new DatagramPacket(buffer, buffer.length, dns, port);
        DatagramPacket old_out = out;
        System.out.println("Contacting server: " + server + ":" + port);
        System.out.println("Resolving: " + lookup);
        while (!answered && cycle < MAXTRIES) {
        	history.add(server);
	        System.out.println("Sending packet of " + buffer.length + " bytes:");
	        System.out.println(msg.identify());
        	try {
        		socket.send(out);
        		socket.receive(received);
		        System.out.println("Received response of " + received.getLength() + " bytes:");
		        if (received.getLength() <= 12)
		        	throw new DecodeException("Recieved packet is header only!",1);
	        	DNSMsg answer = DNSMsg.decode(received.getData(),0,received.getLength());
	        	System.out.println(answer.identify());
	        	try {
	        		answer.checkAnswerTo(msg);
	        		answered = true;
	        	} catch (AnswerException ex) {
			        int id = 0;
			        String new_server = answer.getNextAuthority(id);
			        while (history.contains(new_server)) {
			        	id++;
			        	new_server = answer.getNextAuthority(id);
				        if (new_server.equals("done")) {
				        	System.out.println("Exhausted all possible leads!");
				        	System.out.println("Finished!");
				        	System.exit(1);
				        }
			        }
			        server = new_server;
			        try {
		        		dns = InetAddress.getByName(server);
			        	old_out = out;
				        System.out.println("Contacting server: " + server + ":" + port);
				        System.out.println("Resolving: " + lookup);
			        	out.setAddress(dns);
		        	} catch (UnknownHostException nf) {
			        	System.out.println("Invalid Authority Access!");
			        	dns = old_dns;
			        	out.setAddress(dns);
		        	}
		        } catch (CheckException ex) {
				    System.out.println(ex);
		        	switch (ex.getCls()) {
				        case 1:
				        	System.out.println("Finished!");
				        	System.exit(1);
			        	default:
				        	break;
		        	}
		        }
	        } catch (DecodeException ex) {
	        	/*byte[] tempBytes = received.getData();
	        	byte[] recBytes = new byte[received.getLength()];
	        	for (int index = 0; index < received.getLength(); index++)
	        		recBytes[index] = tempBytes[index];
	        	Hexable answerHex = new Hexable(recBytes);
	        	System.out.println(answerHex.toString());*/
	        	System.out.println(ex);
	        	switch (ex.getCls()) {
		        	case 1:
		        		System.out.println("Finished!");
		        		System.exit(1);
		        	default:
		        		break;
	        	}
	        } catch (SocketTimeoutException ex) {
	        	if (cycle < MAXTRIES - 1)
	        		System.out.println("Resubmitting Packet!");
	        	else
        			System.out.println("Exceeded Maximum Resubmission Attempts!");
	        } finally {
	        	cycle++;
	        }
        }
        System.out.println("Finished!");
    }
}

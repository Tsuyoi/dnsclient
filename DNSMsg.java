import java.util.Random;

public class DNSMsg extends Hexable {
	private int id;
	private boolean qr;
	private int opcode;
	private boolean aa;
	private boolean tc;
	private boolean rd;
	private boolean ra;
	private int zcode;
	private int rcode;
	private int qCount;
	private int aCount;
	private int nsCount;
	private int arCount;
	private Question[] questions;
	private ResourceRecord[] answers;
	private ResourceRecord[] nameservers;
	private ResourceRecord[] additionalResources;
	public DNSMsg() {}
	public DNSMsg(String input) {
		FQDN lookupFQDN = new FQDN(input);
		Question lookupQuestion = new Question(lookupFQDN,1,1);
		Random r = new Random();
		id = r.nextInt(65535);
		qr = false;
		opcode = 0;
		aa = false;
		tc = false;
		rd = false;
		ra = false;
		zcode = 0;
		rcode = 0;
		qCount = 1;
		aCount = 0;
		nsCount = 0;
		arCount = 0;
		questions = new Question[1];
		questions[0] = lookupQuestion;
	}
	public DNSMsg(int ID, Boolean QR, int OPCODE, boolean AA, boolean TC, boolean RD, boolean RA, 
		int ZCODE, int RCODE, int QCOUNT, int ACOUNT, int NSCOUNT, int ARCOUNT, Question[] QUESTIONS, 
		ResourceRecord[] ANSWERS, ResourceRecord[] NAMESERVERS, ResourceRecord[] ADDITIONALRESOURCES) {
		id = ID;
		qr = QR;
		opcode = OPCODE;
		aa = AA;
		tc = TC;
		rd = RD;
		ra = RA;
		zcode = ZCODE;
		rcode = RCODE;
		qCount = QCOUNT;
		aCount = ACOUNT;
		nsCount = NSCOUNT;
		arCount = ARCOUNT;
		questions = QUESTIONS;
		answers = ANSWERS;
		nameservers = NAMESERVERS;
		additionalResources = ADDITIONALRESOURCES;				
	}
	public static DNSMsg decode(byte[] input, int offset, int length) throws DecodeException {
		int idx = offset;
		try {
			int ID = 0;
			for (int index = 0; index < 2; index++) {
				int shift = (1 - index) * 8;
				ID += (input[idx+index] & 0x00FF) << shift;
			}
			idx += 2;
			char tempByte = (char)input[idx];
			boolean QR = false;
			if (((tempByte & 0x80) >>> 7) == 1)
				QR = true;
			int OPCODE = (tempByte & 0x78) >>> 3;
			boolean AA = false;
			if (((tempByte & 0x04) >>> 2) == 1)
				AA = true;
			boolean TC = false;
			if (((tempByte & 0x02) >>> 1) == 1)
				TC = true;
			boolean RD = false;
			if ((tempByte & 0x01) == 1)
				RD = true;
			idx++;
			tempByte = (char)input[idx];
			boolean RA = false;
			if (((tempByte & 0x80) >>> 7) == 1)
				RA = true;
			int ZCODE = (tempByte & 0x70) >>> 3;
			int RCODE = (tempByte & 0x0F);
			switch (RCODE) {
				case 1:
					throw new DecodeException("Response Code indicates a Formatting Error!",1);
				case 2:
					throw new DecodeException("Response Code indicates a Server Failure!",1);
				case 3:
					throw new DecodeException("Response Code indicates a Query for a Non-Existent Domain!",1);
				case 4:
					throw new DecodeException("Response Code indicates this feature is not implemented!",1);
				case 5:
					throw new DecodeException("Response Code indicates this Query was Refused",1);
				default:
					break;
			}
			idx++;
			int QCOUNT = 0;
			for (int index = 0; index < 2; index++)
				QCOUNT += (input[idx+index] & 0x00FF) << ((1 - index) * 8);
			idx += 2;
			int ACOUNT = 0;
			for (int index = 0; index < 2; index++)
				ACOUNT += (input[idx+index] & 0x00FF) << ((1 - index) * 8);
			idx += 2;
			int NSCOUNT = 0;
			for (int index = 0; index < 2; index++)
				NSCOUNT += (input[idx+index] & 0x00FF) << ((1 - index) * 8);
			idx += 2;
			int ARCOUNT = 0;
			for (int index = 0; index < 2; index++)
				ARCOUNT += (input[idx+index] & 0x00FF) << ((1 - index) * 8);
			if (ARCOUNT > NSCOUNT)
				ARCOUNT = NSCOUNT;
			idx += 2;
			Question[] QUESTIONS = new Question[QCOUNT];
			int temp_count = 0;
			for (int counter = 0; counter < QCOUNT; counter++) {
				if (idx > length) {
					QCOUNT = temp_count;
					ACOUNT = 0;
					NSCOUNT = 0;
					ARCOUNT = 0;
					break;
				} else
					temp_count++;
				int questionLength = 0;
				tempByte = (char)input[idx];
				if ((tempByte & 0xC0) == 192) {
					questionLength += 2;
				} else {
					while (input[questionLength+idx] > 0)
						questionLength++;
					questionLength++;
				}
				questionLength += 4;
				QUESTIONS[counter] = Question.decode(input,idx,questionLength);
				idx += questionLength;
			}
			temp_count = 0;
			ResourceRecord[] ANSWERS = new ResourceRecord[ACOUNT];
			for (int counter = 0; counter < ACOUNT; counter++) {
				if (idx > length) {
					ACOUNT = temp_count;
					NSCOUNT = 0;
					ARCOUNT = 0;
					break;
				} else
					temp_count++;
				int answerLength = 0;
				tempByte = (char)input[idx];
				if ((tempByte & 0xC0) == 192) {
					answerLength += 2;
				} else {
					while (input[answerLength+idx] > 0)
						answerLength++;
					answerLength++;
				}
				int type = 0;
				for (int index = 0; index < 2; index++)
					type += (input[answerLength+idx+index] & 0x00FF) << ((1 - index) * 8);
				answerLength += 8;
				int rdLen = 0;
				for (int index = 0; index < 2; index++)
					rdLen += (input[idx+answerLength+index] & 0x00FF) << ((1 - index) * 8);
				answerLength += 2;
				if (type == 2) {
					rdLen = 0;
					boolean ended = false;
					while (!ended) {
						tempByte = (char)input[idx+answerLength+rdLen];
						if ((tempByte & 0xC0) == 192) {
							rdLen += 2;
							ended = true;
						} else {
							int size = (int)input[idx+answerLength+rdLen];
							if (size > 0)
								rdLen += size+1;
							else {
								rdLen++;
								ended = true;
							}
						}
					}
				}
				answerLength += rdLen;
				ANSWERS[counter] = ResourceRecord.decode(input,idx,answerLength);
				idx += answerLength;
			}
			temp_count = 0;
			ResourceRecord[] NAMESERVERS = new ResourceRecord[NSCOUNT];
			for (int counter = 0; counter < NSCOUNT; counter++) {
				if (idx > length) {
					NSCOUNT = temp_count;
					ARCOUNT = 0;
					break;
				} else
					temp_count++;
				int nameserverLength = 0;
				tempByte = (char)input[idx];
				if ((tempByte & 0xC0) == 192) {
					nameserverLength += 2;
				} else {
					while (input[nameserverLength+idx] > 0)
						nameserverLength++;
					nameserverLength++;
				}
				int type = 0;
				for (int index = 0; index < 2; index++)
					type += (input[nameserverLength+idx+index] & 0x00FF) << ((1 - index) * 8);
				nameserverLength += 8;
				int rdLen = 0;
				for (int index = 0; index < 2; index++)
					rdLen += (input[idx+nameserverLength+index] & 0x00FF) << ((1 - index) * 8);
				nameserverLength += 2;
				if (type == 2) {
					rdLen = 0;
					boolean ended = false;
					while (!ended) {
						tempByte = (char)input[idx+nameserverLength+rdLen];
						if ((tempByte & 0xC0) == 192) {
							rdLen += 2;
							ended = true;
						} else {
							int size = (int)input[idx+nameserverLength+rdLen];
							if (size > 0)
								rdLen += size+1;
							else {
								rdLen++;
								ended = true;
							}
						}
					}
				}
				nameserverLength += rdLen;
				NAMESERVERS[counter] = ResourceRecord.decode(input,idx,nameserverLength);
				idx += nameserverLength;
			}
			temp_count = 0;
			ResourceRecord[] ADDITIONALRESOURCES = new ResourceRecord[ARCOUNT];
			for (int counter = 0; counter < ARCOUNT; counter++) {
				if (idx > length) {
					ARCOUNT = temp_count;
					break;
				} else
					temp_count++;
				int additionalResourceLength = 0;
				tempByte = (char)input[idx];
				if ((tempByte & 0xC0) == 192) {
					additionalResourceLength += 2;
				} else {
					while (input[additionalResourceLength+idx] > 0)
						additionalResourceLength++;
					additionalResourceLength++;
				}
				int type = 0;
				for (int index = 0; index < 2; index++)
					type += (input[additionalResourceLength+idx+index] & 0x00FF) << ((1 - index) * 8);
				additionalResourceLength += 8;
				int rdLen = 0;
				for (int index = 0; index < 2; index++)
					rdLen += (input[idx+additionalResourceLength+index] & 0x00FF) << ((1 - index) * 8);
				additionalResourceLength += 2;
				if (type == 2) {
					rdLen = 0;
					boolean ended = false;
					while (!ended) {
						tempByte = (char)input[idx+additionalResourceLength+rdLen];
						if ((tempByte & 0xC0) == 192) {
							rdLen += 2;
							ended = true;
						} else {
							int size = (int)input[idx+additionalResourceLength+rdLen];
							if (size > 0)
								rdLen += size+1;
							else {
								rdLen++;
								ended = true;
							}
						}
					}
				}
				additionalResourceLength += rdLen;
				ADDITIONALRESOURCES[counter] = ResourceRecord.decode(input,idx,additionalResourceLength);
				idx += additionalResourceLength;
			}
			return new DNSMsg(ID,QR,OPCODE,AA,TC,RD,RA,ZCODE,RCODE,QCOUNT,ACOUNT,NSCOUNT,
				ARCOUNT,QUESTIONS,ANSWERS,NAMESERVERS,ADDITIONALRESOURCES);
		} catch (DecodeException ex) {
			throw ex;
		} catch (ArrayIndexOutOfBoundsException ex) {
			throw new DecodeException("Error decoding DNSMsg starting at byte: " + idx + " (" + length + " Bytes)");
		}
	}
	@Override
	public byte[] encode() {
		int questionBytes = 0;
		for (int counter = 0; counter < qCount; counter++) {
			byte[] temp = questions[counter].encode();
			questionBytes += temp.length;
		}
		int answerBytes = 0;
		for (int counter = 0; counter < aCount; counter++) {
			byte[] temp = answers[counter].encode();
			answerBytes += temp.length;
		}
		int nameserverBytes = 0;
		for (int counter = 0; counter < nsCount; counter++) {
			byte[] temp = nameservers[counter].encode();
			nameserverBytes += temp.length;
		}
		int additionalResourceBytes = 0;
		for (int counter = 0; counter < arCount; counter++) {
			byte[] temp = additionalResources[counter].encode();
			additionalResourceBytes += temp.length;
		}
		byte[] ret = new byte[questionBytes+12];
		for (int index = 0; index < 2; index++) 
			ret[index] = (byte)(id >>> ((1-index) * 8));
		int temp = 0;
		if (rd)
			temp += 1;
		if (tc)
			temp += 2;
		if (aa)
			temp += 4;
		temp += opcode * 8;
		if (qr)
			temp += 128;
		ret[2] = (byte)temp;
		temp = rcode;
		temp += zcode*32;
		if (ra)
			temp += 128;
		ret[3] = (byte)temp;
		int offset = 4;
		for (int index = 0; index < 2; index++)
			ret[offset+index] = (byte)(qCount >>> ((1-index) * 8));
		offset = 6;
		for (int index = 0; index < 2; index++)
			ret[offset+index] = (byte)(aCount >>> ((1-index) * 8));
		offset = 8;
		for (int index = 0; index < 2; index++)
			ret[offset+index] = (byte)(nsCount >>> ((1-index) * 8));
		offset = 10;
		for (int index = 0; index < 2; index++)
			ret[offset+index] = (byte)(arCount >>> ((1-index) * 8));
		offset = 12;
		for (int counter = 0; counter < qCount; counter++) {
			byte[] tempQuestion = questions[counter].encode();
			for (int index = 0; index < tempQuestion.length; index++) {
				ret[offset+index] = tempQuestion[index];
			}
			offset += tempQuestion.length;
		}
		for (int counter = 0; counter < aCount; counter++) {
			byte[] tempAnswer = answers[counter].encode();
			for (int index = 0; index < tempAnswer.length; index++) {
				ret[offset+index] = tempAnswer[index];
			}
			offset += tempAnswer.length;
		}
		for (int counter = 0; counter < nsCount; counter++) {
			byte[] tempNameserver = nameservers[counter].encode();
			for (int index = 0; index < tempNameserver.length; index++) {
				ret[offset+index] = tempNameserver[index];
			}
			offset += tempNameserver.length;
		}
		for (int counter = 0; counter < arCount; counter++) {
			byte[] tempAdditionalResource = additionalResources[counter].encode();
			for (int index = 0; index < tempAdditionalResource.length; index++) {
				ret[offset+index] = tempAdditionalResource[index];
			}
			offset += tempAdditionalResource.length;
		}
		return ret;
	}
	public int getID() { return id; }
	public String identify() {
		String ret = new String();
		int RD = 0;
		if (rd) { RD = 1; }
		int QR = 0;
		if (qr) {
			QR = 1;
			ret = new String("ReqID=" + id + "/RD=" + RD + "/Rsp=" + QR + "/QType=" + 
				opcode + "/Result=" + rcode + "  " + qCount + "Q/" + aCount + "Ans/" + 
				nsCount + "Auth/" + arCount + "Addl");
		} else {
			ret = new String("ReqID=" + id + "/RD=" + RD + "/Rsp=" + QR + "/QType=" + 
				opcode + "  " + qCount + "Q/" + aCount + "Ans/" + nsCount + "Auth/" + arCount + "Addl");
		}
		for (int counter = 0; counter < qCount; counter++)
			ret = ret + "\nQuestion: " + questions[counter].identify();
		for (int counter = 0; counter < aCount; counter++)
			ret = ret + "\nAnswer: " + answers[counter].identify();
		for (int counter = 0; counter < nsCount; counter++)
			ret = ret + "\nAuthority: " + nameservers[counter].identify();
		for (int counter = 0; counter < arCount; counter++)
			ret = ret + "\nAdd't Info: " + additionalResources[counter].identify();
		return ret;
	}
	public void checkAnswerTo(DNSMsg query) throws CheckException, AnswerException {
		if (!qr) { throw new CheckException("Not A Response!"); }
		if (id != query.getID()) { throw new CheckException("Invalid ID!"); }
		switch (rcode) {
			case 0:
				break;
			case 1:
				throw new CheckException("RCODE Indicates Format Error!");
			case 2:
				throw new CheckException("RCODE Indicates Server Fault!");
			case 3:
				if (aa)
					throw new CheckException("RCODE Indicates Does Not Exist!");
				break;
			default:
				break;
		}
		if (zcode != 0) { throw new CheckException("Invalid ZCODE Received!"); }
		if (qCount == 0) { throw new CheckException("Question is Missing!"); }
		if (!query.getQuestionFQDN().equals(questions[0].getQuestionFQDN())) {
			throw new CheckException("Questions Do Not Match!");
		}
		if (aCount == 0) {
			if (arCount > 0)
				throw new AnswerException(additionalResources[0].getRData(), 53);
			else {
				if (nsCount > 0) {
					throw new AnswerException(nameservers[0].getRData(), 53);
				}
				throw new CheckException("No Answer and No Further Resources!",1);
			}
		}
		if (!aa)
			System.out.println(answers[0].getRData() + " --> " + questions[0].getQuestionFQDN());
		else
			System.out.println(answers[0].getRData() + " --> " + questions[0].getQuestionFQDN() + 
				" (Authoritative Answer)");
	}
	public String getNextAuthority(int id) {
		String ret = new String("done");
			if (arCount > 0) {
				if (id < arCount) {
					return additionalResources[id].getRData();
				} else {
					if (nsCount > 0) {
						if (id < nsCount) {
							return nameservers[id].getRData();
						}
					}
				}
			} else {
				if (nsCount > 0) {
					if (id < nsCount) {
						return nameservers[id].getRData();
					}
				}
			}
		return ret;
	}
	public String getQuestionFQDN() {
		return questions[0].getQuestionFQDN();
	}
}
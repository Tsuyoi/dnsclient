/*
 *  Hexable Class to provide method to display byte encoding in hex format
 */
public class Hexable {
	private byte[] temporaryData;
    // Constructor
    public Hexable(byte[] input) {
    	temporaryData = new byte[input.length];
    	for (int index = 0; index < input.length; index++) {
	    	temporaryData[index] = input[index];
    	}
    }
    public Hexable() {
    	temporaryData = new byte[0];
    }
    // Return an empty byte array to prevent failure if called from this class
    public byte[] encode() {
        return temporaryData;
    }
    /*
     *  toString Method
     *    Returns a hex String representation of the byte array produced
     *    by the encode method
     */
    public String toString() {
        final char[] hex = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
        byte[] source = this.encode();
        char[] ret = new char[source.length * 2];
        int temp;
        for (int index = 0; index < source.length; index++) {
            temp = source[index] & 0xFF;
            ret[index * 2] = hex[temp >>> 4];
            ret[index * 2 + 1] = hex[temp & 0x0F];
        }
        int index = 0;
        int stage = 0;
        String result = new String();
        while (index < ret.length) {
            for (int i = 0; i < 2; i++) {
                result = result + ret[index];
                index++;
            }
            result = result + " ";
            if (stage%4 == 3)
            	result = result + " ";
            if (stage%16 == 15)
            	result = result + "\n";
            stage++;
        }
        return result;
    }
}

JFLAGS = -g
JC = javac
.SUFFIXES: .java .class

.java.class:
	$(JC) $(JFLAGS) $*.java

CLASSES = \
	AnswerException.java \
	CheckException.java \
	DecodeException.java \
	Hexable.java \
	FQDN.java \
	Question.java \
	ResourceRecord.java \
	DNSMsg.java \
	DNSClient.java

default: classes

classes: $(CLASSES:.java=.class)

clean:
	$(RM) *.class
